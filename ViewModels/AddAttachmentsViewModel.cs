﻿using System;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using DynamicData.Binding;
using ReactiveUI;

namespace MKVToolNixAutomation.ViewModels;

public class AddAttachmentsViewModel : ViewModelBase, ITabbed
{
	public string TabHeader => "Add attachments";

	private readonly ObservableAsPropertyHelper<string> videosString;
	public string VideosString => videosString.Value;

	private string? attachmentsDirectoryPath;

	public string? AttachmentsDirectoryPath
	{
		get => attachmentsDirectoryPath;
		private set => this.RaiseAndSetIfChanged(ref attachmentsDirectoryPath, value);
	}

	private string? chaptersFileName = "chapters.xml";

	public string? ChaptersFileName
	{
		get => chaptersFileName;
		private set => this.RaiseAndSetIfChanged(ref chaptersFileName, value);
	}

	private string? tagsFileName = "tags.xml";

	public string? TagsFileName
	{
		get => tagsFileName;
		private set => this.RaiseAndSetIfChanged(ref tagsFileName, value);
	}

	private string? optionalArguments;

	public string? OptionalArguments
	{
		get => optionalArguments;
		private set => this.RaiseAndSetIfChanged(ref optionalArguments, value);
	}

	private string[] videoFilePaths = Array.Empty<string>();

	public string[] VideoFilePaths
	{
		get => videoFilePaths;
		private set
		{
			if (value.Any(string.IsNullOrWhiteSpace) || value.SequenceEqual(videoFilePaths)) return;
			videoFilePaths = value;
			this.RaisePropertyChanged(nameof(VideoFilePaths));
		}
	}

	public AddAttachmentsViewModel()
	{
		videosString =
			this.WhenValueChanged(x => x.VideoFilePaths)
			    .Select(files => string.Join(", ", (files ?? Array.Empty<string>()).Select(Path.GetFileName)))
			    .ToProperty(this, x => x.VideosString, string.Empty);
	}

	public async Task SelectVideos()
	{
		var dialog = new OpenFileDialog
		{
			AllowMultiple = true,
			Filters = { new FileDialogFilter { Name = "Matroska files", Extensions = { "mkv" } } }
		};

		if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
		{
			string[]? result = await dialog.ShowAsync(desktop.MainWindow);
			if (result != null) VideoFilePaths = result;
		}
	}

	public async Task SelectAttachments()
	{
		var dialog = new OpenFolderDialog();

		if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
		{
			AttachmentsDirectoryPath = await dialog.ShowAsync(desktop.MainWindow);
		}
	}
}