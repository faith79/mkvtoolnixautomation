﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using MKVToolNixAutomation.Data;
using MKVToolNixAutomation.Helpers;
using MKVToolNixAutomation.Models;
using DynamicData.Binding;
using MKVToolNixAutomation.Enums;
using ReactiveUI;

namespace MKVToolNixAutomation.ViewModels;

public class SetDefaultTracksViewModel : ViewModelBase, ITabbed
{
	public string TabHeader => "Set default tracks";

	private ReactiveCommand<Unit, ObservableCollection<ContainerMetadata>> LoadMetadata { get; }
	private ReactiveCommand<Unit, Unit> InitDefaultTracks { get; }

	private readonly ObservableAsPropertyHelper<string> videosString;
	public string VideosString => videosString.Value;

	private readonly ObservableAsPropertyHelper<ObservableCollection<ContainerMetadata>> containersMetadata;
	public ObservableCollection<ContainerMetadata> ContainersMetadata => containersMetadata.Value;

	private readonly ObservableAsPropertyHelper<bool> isEnabled;
	public bool IsEnabled => isEnabled.Value;

	private readonly ObservableAsPropertyHelper<List<Track>> videoTracks;
	public List<Track> VideoTracks => videoTracks.Value;

	private readonly ObservableAsPropertyHelper<List<Track>> audioTracks;
	public List<Track> AudioTracks => audioTracks.Value;

	private readonly ObservableAsPropertyHelper<List<Track>> subtitlesTracks;
	public List<Track> SubtitlesTracks => subtitlesTracks.Value;

	private Track? defaultVideoTrack;

	public Track? DefaultVideoTrack
	{
		get => defaultVideoTrack;
		private set => this.RaiseAndSetIfChanged(ref defaultVideoTrack, value);
	}

	private Track? defaultAudioTrack;

	public Track? DefaultAudioTrack
	{
		get => defaultAudioTrack;
		private set => this.RaiseAndSetIfChanged(ref defaultAudioTrack, value);
	}

	private Track? defaultSubtitlesTrack;

	public Track? DefaultSubtitlesTrack
	{
		get => defaultSubtitlesTrack;
		private set => this.RaiseAndSetIfChanged(ref defaultSubtitlesTrack, value);
	}

	private bool force;

	public bool Force
	{
		get => force;
		private set => this.RaiseAndSetIfChanged(ref force, value);
	}

	private string[] videoFilePaths = Array.Empty<string>();

	public string[] VideoFilePaths
	{
		get => videoFilePaths;
		private set
		{
			if (value.Any(string.IsNullOrWhiteSpace) || value.SequenceEqual(videoFilePaths)) return;
			videoFilePaths = value;
			this.RaisePropertyChanged(nameof(VideoFilePaths));
		}
	}

	public SetDefaultTracksViewModel(MKVToolNix mkvtoolnix)
	{
		var analyzer = new Analyzer(mkvtoolnix);

		LoadMetadata = ReactiveCommand.CreateFromTask(async () =>
		{
			var metadataList = await analyzer.Load(VideoFilePaths);
			return new ObservableCollection<ContainerMetadata>(metadataList);
		});
		LoadMetadata.ThrownExceptions.Subscribe(ex => Logger.WriteLine(ex.ToString(), LogLevel.Error));
		isEnabled = LoadMetadata.IsExecuting.ToProperty(this, x => x.IsEnabled);

		InitDefaultTracks = ReactiveCommand.Create(() =>
		{
			DefaultVideoTrack = Analyzer.GetDefaultTrack(VideoTracks);
			DefaultAudioTrack = Analyzer.GetDefaultTrack(AudioTracks);
			DefaultSubtitlesTrack = Analyzer.GetDefaultTrack(SubtitlesTracks);
		});
		InitDefaultTracks.ThrownExceptions.Subscribe(ex => Logger.WriteLine(ex.ToString(), LogLevel.Error));

		containersMetadata =
			LoadMetadata.ToProperty(this, x => x.ContainersMetadata, scheduler: RxApp.MainThreadScheduler);

		videosString =
			this.WhenValueChanged(x => x.VideoFilePaths)
			    .Select(files => string.Join(", ", (files ?? Array.Empty<string>()).Select(Path.GetFileName)))
			    .ToProperty(this, x => x.VideosString);

		videoTracks =
			this.WhenValueChanged(x => x.ContainersMetadata)
			    .Select(metadataList => Analyzer.GetTracksOfType(metadataList, TrackType.Video))
			    .ToProperty(this, x => x.VideoTracks);

		audioTracks =
			this.WhenValueChanged(x => x.ContainersMetadata)
			    .Select(metadataList => Analyzer.GetTracksOfType(metadataList, TrackType.Audio))
			    .ToProperty(this, x => x.AudioTracks);

		subtitlesTracks =
			this.WhenValueChanged(x => x.ContainersMetadata)
			    .Select(metadataList => Analyzer.GetTracksOfType(metadataList, TrackType.Subtitles))
			    .ToProperty(this, x => x.SubtitlesTracks);

		this.WhenValueChanged(x => x.VideoFilePaths)
		    .Select(_ => Unit.Default)
		    .InvokeCommand(this, x => x.LoadMetadata);

		this.WhenValueChanged(x => x.ContainersMetadata)
		    .Select(_ => Unit.Default)
		    .InvokeCommand(this, x => x.InitDefaultTracks);
	}

	public async Task SelectVideos()
	{
		var dialog = new OpenFileDialog
		{
			AllowMultiple = true,
			Filters = { new FileDialogFilter { Name = "Matroska files", Extensions = { "mkv" } } }
		};

		if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
		{
			string[]? result = await dialog.ShowAsync(desktop.MainWindow);
			if (result != null) VideoFilePaths = result;
		}
	}
}