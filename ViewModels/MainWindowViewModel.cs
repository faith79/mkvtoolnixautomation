﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using MKVToolNixAutomation.Data;
using MKVToolNixAutomation.Enums;
using MKVToolNixAutomation.Helpers;
using ReactiveUI;

namespace MKVToolNixAutomation.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
	private ITabbed selectedTab = null!;

	private ReactiveCommand<Unit, Unit> Run { get; }

	private readonly ObservableAsPropertyHelper<bool> isEnabled;
	public bool IsEnabled => isEnabled.Value;

	private string? directory;

	public string? Directory
	{
		get => directory;
		private set => this.RaiseAndSetIfChanged(ref directory, value);
	}

	public ITabbed SelectedTab
	{
		get => selectedTab;
		private set => this.RaiseAndSetIfChanged(ref selectedTab, value);
	}

	private ObservableCollection<ViewModelBase> tabItems = new();

	public ObservableCollection<ViewModelBase> TabItems
	{
		get => tabItems;
		private set => this.RaiseAndSetIfChanged(ref tabItems, value);
	}

	private readonly MKVToolNix mkvtoolnix;

	public MainWindowViewModel()
	{
		mkvtoolnix = new MKVToolNix();
		var worker = new Worker(mkvtoolnix);

		TabItems.Add(new SetDefaultTracksViewModel(mkvtoolnix));
		TabItems.Add(new AddAttachmentsViewModel());

		Run = ReactiveCommand.CreateFromTask(async () =>
		{
			switch (SelectedTab)
			{
				case SetDefaultTracksViewModel vm:
					await worker.SetDefaultTracks(vm);
					break;
				case AddAttachmentsViewModel vm:
					await worker.AddAttachments(vm);
					break;
			}
		});

		isEnabled = Run.IsExecuting
		               .Merge(this.WhenAnyValue(x => x.Directory).Select(string.IsNullOrEmpty))
		               .ToProperty(this, x => x.IsEnabled);

		string? appDirectory = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule?.FileName);
		SetDir(appDirectory);
	}

	public async Task SelectMKVToolNixDirectory()
	{
		var dialog = new OpenFolderDialog();

		if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
		{
			SetDir(await dialog.ShowAsync(desktop.MainWindow));
		}
	}

	private void SetDir(string? dir)
	{
		try
		{
			mkvtoolnix.Directory = dir;
			Directory = mkvtoolnix.Directory;
		}
		catch (MKVException ex)
		{
			Logger.WriteLine(ex.ToString(), LogLevel.Error);
		}
	}
}