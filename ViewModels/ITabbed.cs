﻿namespace MKVToolNixAutomation.ViewModels;

public interface ITabbed
{
	string TabHeader { get; }
}