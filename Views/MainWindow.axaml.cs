using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using AvaloniaEdit;
using MKVToolNixAutomation.Helpers;

namespace MKVToolNixAutomation.Views;

public class MainWindow : Window
{
	public MainWindow()
	{
		InitializeComponent();
#if DEBUG
		this.AttachDevTools();
#endif
		Logger.Initialize(this.FindControl<TextEditor>("Log"));
	}

	private void InitializeComponent()
	{
		AvaloniaXamlLoader.Load(this);
	}
}