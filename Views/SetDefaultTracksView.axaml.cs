using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace MKVToolNixAutomation.Views;

public class SetDefaultTracksView : UserControl
{
	public SetDefaultTracksView()
	{
		InitializeComponent();
	}

	private void InitializeComponent()
	{
		AvaloniaXamlLoader.Load(this);
	}
}