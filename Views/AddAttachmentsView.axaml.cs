using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace MKVToolNixAutomation.Views;

public class AddAttachmentsView : UserControl
{
	public AddAttachmentsView()
	{
		InitializeComponent();
	}

	private void InitializeComponent()
	{
		AvaloniaXamlLoader.Load(this);
	}
}