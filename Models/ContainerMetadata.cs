﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using MKVToolNixAutomation.Enums;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CollectionNeverUpdated.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace MKVToolNixAutomation.Models;

#nullable disable
public class ContainerMetadata
{
	[JsonPropertyName("file_name")]
	public string FilePath { get; init; }

	[JsonPropertyName("tracks")]
	public List<Track> Tracks { get; init; } = new();
}

public partial class Track
{
	[JsonPropertyName("id")]
	public int Id { get; init; }

	[JsonPropertyName("codec")]
	public string Codec { get; init; }

	[JsonPropertyName("type")]
	public TrackType Type { get; init; }

	[JsonPropertyName("properties")]
	public TrackProperties Properties { get; init; }
}
#nullable restore

public class TrackProperties
{
	[JsonPropertyName("default_track")]
	public bool IsDefault { get; init; }

	[JsonPropertyName("language")]
	public string? Language { get; init; }

	[JsonPropertyName("number")]
	public int Number { get; init; }

	[JsonPropertyName("track_name")]
	public string? Name { get; init; }
}

public partial class Track
{
	public bool IsEmpty => Type == TrackType.Empty;

	public override string ToString()
	{
		string trackName = string.IsNullOrEmpty(Properties.Name) ? "untitled" : Properties.Name;
		string trackLang = string.IsNullOrEmpty(Properties.Language) ? "undefined" : Properties.Language;

		return Type switch
		{
			TrackType.Empty     => "None",
			TrackType.Video     => $"{Id}: {Codec}",
			TrackType.Audio     => $"{Id}: {Codec} [{trackLang}]",
			TrackType.Subtitles => $"{Id}: {trackName} [{trackLang}]",
			_                   => $"{Id}"
		};
	}

	public static Track Empty()
	{
		return new Track
		{
			Id = -1,
			Codec = string.Empty,
			Type = TrackType.Empty,
			Properties = new TrackProperties
			{
				IsDefault = false,
				Number = -1
			}
		};
	}
}