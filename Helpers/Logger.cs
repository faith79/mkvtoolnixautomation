﻿using System;
using System.Diagnostics;
using Avalonia.Media;
using AvaloniaEdit;
using AvaloniaEdit.Highlighting;
using MKVToolNixAutomation.Enums;

namespace MKVToolNixAutomation.Helpers;

public static class Logger
{
	private static TextEditor textEditor = null!;
	private static readonly RichTextModel RichTextModel = new();

	public static void Initialize(TextEditor editor)
	{
		textEditor = editor;
		textEditor.TextArea.TextView.LineTransformers.Add(new RichTextColorizer(RichTextModel));
	}

	public static void WriteLine(string text, LogLevel logLevel)
	{
		string s = text + Environment.NewLine;
		AppendText(s);

		var color = logLevel switch
		{
			LogLevel.Warning => Color.FromRgb(225, 225, 100),
			LogLevel.Error   => Color.FromRgb(225, 100, 100),
			LogLevel.Success => Color.FromRgb(75, 225, 75),
			_                => Color.FromRgb(204, 204, 204)
		};

		RichTextModel.ApplyHighlighting
		(
			textEditor.Text.Length - s.Length,
			s.Length,
			new HighlightingColor { Foreground = new SimpleHighlightingBrush(color) }
		);
	}

	public static void WriteTime(Stopwatch sw)
	{
		string s = $"Task completed after {sw.ElapsedMilliseconds}ms";
		WriteLine(s, LogLevel.Success);
	}

	private static void AppendText(string text)
	{
		textEditor.AppendText(text);
		textEditor.ScrollToEnd();
	}
}