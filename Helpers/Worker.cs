﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MKVToolNixAutomation.Data;
using MKVToolNixAutomation.ViewModels;
using Avalonia.Threading;
using MKVToolNixAutomation.Enums;

namespace MKVToolNixAutomation.Helpers;

public class Worker
{
	private readonly MKVToolNix mkvtoolnix;

	public Worker(MKVToolNix mkvtoolnix)
	{
		this.mkvtoolnix = mkvtoolnix;
	}

	public async Task SetDefaultTracks(SetDefaultTracksViewModel data)
	{
		try
		{
			var sw = Stopwatch.StartNew();

			// this is guaranteed to have at least one element
			int totalTracks = data.ContainersMetadata.First().Tracks.Count;

			foreach (var container in data.ContainersMetadata)
			{
				if (container.Tracks.Count != totalTracks)
				{
					Logger.WriteLine($"[Skipping] Mismatching tracks for `{Path.GetFileName(container.FilePath)}`", LogLevel.Warning);
					continue;
				}

				string args = ArgBuilder.Generate(container, data.DefaultVideoTrack, data.DefaultAudioTrack, data.DefaultSubtitlesTrack, data.Force);
				using var mkvpropedit = new Process
				{
					StartInfo =
					{
						FileName = mkvtoolnix.PropEdit,
						RedirectStandardOutput = true,
						CreateNoWindow = true,
						UseShellExecute = false,
						Arguments = args
					}
				};
				mkvpropedit.OutputDataReceived += RedirectOutput;
				mkvpropedit.Start();

				mkvpropedit.BeginOutputReadLine();
				await mkvpropedit.WaitForExitAsync();
			}

			sw.Stop();
			Logger.WriteTime(sw);
		}
		catch (Exception ex)
		{
			Logger.WriteLine(ex.ToString(), LogLevel.Error);
		}
	}

	public async Task AddAttachments(AddAttachmentsViewModel data)
	{
		try
		{
			var sw = Stopwatch.StartNew();

			var collector = new Collector(data.VideoFilePaths, data.AttachmentsDirectoryPath, data.ChaptersFileName, data.TagsFileName);
			var containers = collector.FetchAttachments();

			// this is guaranteed to have at least one element and an actual value
			string first = containers.First().Video!;
			string dirName = Utils.GenerateOutputDirectoryName(first);
			string outputDir = Path.Combine(Path.GetDirectoryName(first)!, dirName);
			Directory.CreateDirectory(outputDir);

			foreach (var container in containers)
			{
				string args = ArgBuilder.Generate(container, data.OptionalArguments, outputDir);

				using var mkvmerge = new Process
				{
					StartInfo =
					{
						FileName = mkvtoolnix.Merge,
						RedirectStandardOutput = true,
						CreateNoWindow = true,
						UseShellExecute = false,
						Arguments = args
					}
				};
				mkvmerge.OutputDataReceived += RedirectOutput;
				mkvmerge.Start();

				mkvmerge.BeginOutputReadLine();
				await mkvmerge.WaitForExitAsync();
			}

			sw.Stop();
			Logger.WriteTime(sw);
		}
		catch (Exception ex)
		{
			Logger.WriteLine(ex.ToString(), LogLevel.Error);
		}
	}

	private static void RedirectOutput(object sender, DataReceivedEventArgs e)
	{
		if (e.Data == null) return;
		var logLevel = e.Data.StartsWith("Error") ? LogLevel.Error : LogLevel.Info;
		Dispatcher.UIThread.InvokeAsync(() => Logger.WriteLine(e.Data, logLevel));
	}
}