﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MKVToolNixAutomation.Enums;
using MKVToolNixAutomation.Helpers;
using MKVToolNixAutomation.Models;

namespace MKVToolNixAutomation.Data;

public class Collector
{
	private readonly string[] videoFilePaths;
	private readonly string attachmentsDirectoryPath;
	private readonly string chaptersFileName;
	private readonly string tagsFileName;

	public Collector(string[] videoFilePaths, string? attachmentsDirectoryPath, string? chaptersFileName, string? tagsFileName)
	{
		if (videoFilePaths.Length == 0) throw new MKVException(ErrorType.VideosNotSet);
		if (string.IsNullOrWhiteSpace(attachmentsDirectoryPath)) throw new MKVException(ErrorType.AttachmentsDirNotSet);
		if (string.IsNullOrWhiteSpace(chaptersFileName)) throw new MKVException(ErrorType.ChaptersFileNameEmpty);
		if (string.IsNullOrWhiteSpace(tagsFileName)) throw new MKVException(ErrorType.TagsFileNameEmpty);

		this.attachmentsDirectoryPath = attachmentsDirectoryPath;
		this.videoFilePaths = videoFilePaths;
		this.chaptersFileName = chaptersFileName;
		this.tagsFileName = tagsFileName;
	}

	public List<ContainerAttachments> FetchAttachments()
	{
		var attachmentsDirectory = new DirectoryInfo(attachmentsDirectoryPath);
		var structure = GetDirectoryStructure(attachmentsDirectory);

		var containers = structure switch
		{
			DirectoryStructure.A => new List<ContainerAttachments> { FetchAttachmentsA(attachmentsDirectoryPath) },
			DirectoryStructure.B => new List<ContainerAttachments> { FetchAttachmentsB(attachmentsDirectoryPath) },
			DirectoryStructure.C => FetchAttachmentsC(attachmentsDirectoryPath),
			DirectoryStructure.D => FetchAttachmentsD(attachmentsDirectoryPath),
			DirectoryStructure.E => FetchAttachmentsE(attachmentsDirectoryPath),
			DirectoryStructure.F => FetchAttachmentsF(attachmentsDirectoryPath),
			_                    => new List<ContainerAttachments>()
		};

		if (containers.Count != videoFilePaths.Length) throw new MKVException(ErrorType.VideosAttachmentsMismatch);

		for (int i = 0; i < videoFilePaths.Length; i++)
			containers[i].Video = videoFilePaths[i];

		return containers;
	}

	private DirectoryStructure GetDirectoryStructure(DirectoryInfo mainDir)
	{
		var dirs = mainDir.GetDirectories();
		var files = mainDir.GetFiles();

		switch (dirs.Length)
		{
			// Does the directory contain files/dirs?
			// If yes, are all files in the main dir without any dirs?
			case 0:
				return files.Any() ? DirectoryStructure.A : throw new MKVException(ErrorType.AttachmentsDirEmpty);

			// Is there only one dir?
			case 1 when
				// If yes, does main dir contain the same amount of files as videos selected?
				files.Length == videoFilePaths.Length: return DirectoryStructure.F;

			// If no, does main dir contain any files at all?
			case 1 when
				files.Any(): return DirectoryStructure.B;

			// If not, does the sub dir contain another dir?
			case 1:
				return dirs[0].GetDirectories().Length switch
				{
					0 => DirectoryStructure.C,
					1 => DirectoryStructure.D,
					_ => throw new MKVException(ErrorType.AttachmentsDirUnknownStructure)
				};

			// Is there more than one dir?
			case > 1:
			{
				int dirsWithSubDir = dirs.Count(Utils.ContainsSingleDirectory);
				int dirsWithFontFiles = dirs.Count(Utils.ContainsFontFiles);

				// Is another dir in each of these dirs?
				if (dirsWithSubDir == dirs.Length)
					return DirectoryStructure.D;

				// If not, are there font files located in each of these dirs?
				if (dirsWithFontFiles == dirs.Length)
					return DirectoryStructure.C;

				// If not, is there a dir that contains all font files?
				if (dirsWithFontFiles == 1)
					return DirectoryStructure.E;

				// None of these dirs should contain another dir
				// If there is a dir in one or more but not all of them, the structure is unknown
				if (dirsWithSubDir == 0)
					return DirectoryStructure.D;
				break;
			}
		}

		throw new MKVException(ErrorType.AttachmentsDirUnknownStructure);
	}

	private ContainerAttachments FetchAttachmentsA(string dir)
	{
		string? chapters = null;
		string? tags = null;
		var subtitles = new List<string>();
		var fonts = new List<string>();

		foreach (string file in Directory.GetFiles(dir))
		{
			if (file.EndsWith(chaptersFileName, StringComparison.OrdinalIgnoreCase))
				chapters = file;
			else if (file.EndsWith(tagsFileName, StringComparison.OrdinalIgnoreCase))
				tags = file;
			else if (Formats.IsFontFile(file))
				fonts.Add(file);
			else if (Formats.IsSubtitleFile(file))
				subtitles.Add(file);
		}

		return new ContainerAttachments(chapters, tags, subtitles, fonts);
	}

	private ContainerAttachments FetchAttachmentsB(string dir)
	{
		string? chapters = null;
		string? tags = null;
		var subtitles = new List<string>();
		var fonts = new List<string>();

		foreach (string file in Directory.GetFiles(dir))
		{
			if (file.EndsWith(chaptersFileName, StringComparison.OrdinalIgnoreCase))
				chapters = file;
			else if (file.EndsWith(tagsFileName, StringComparison.OrdinalIgnoreCase))
				tags = file;
			else if (Formats.IsSubtitleFile(file))
				subtitles.Add(file);
		}

		// Should be just one directory
		foreach (string subDir in Directory.GetDirectories(dir))
		{
			if (Utils.ContainsFontFiles(subDir))
				fonts.AddRange(Directory.GetFiles(subDir));
		}

		return new ContainerAttachments(chapters, tags, subtitles, fonts);
	}

	private List<ContainerAttachments> FetchAttachmentsC(string dir)
	{
		return Directory.GetDirectories(dir).Select(FetchAttachmentsA).ToList();
	}

	private List<ContainerAttachments> FetchAttachmentsD(string dir)
	{
		return Directory.GetDirectories(dir).Select(FetchAttachmentsB).ToList();
	}

	private List<ContainerAttachments> FetchAttachmentsE(string dir)
	{
		var mkvs = new List<ContainerAttachments>();
		var fonts = new List<string>();

		string[] subDirs = Directory.GetDirectories(dir);
		foreach (string subDir in subDirs)
		{
			if (Utils.ContainsFontFiles(subDir))
				fonts.AddRange(Directory.GetFiles(subDir));
		}

		foreach (string subDir in subDirs)
		{
			string? chapters = null;
			string? tags = null;
			var subtitles = new List<string>();

			if (Utils.ContainsFontFiles(subDir))
				continue;

			foreach (string file in Directory.GetFiles(subDir))
			{
				if (file.EndsWith(chaptersFileName, StringComparison.OrdinalIgnoreCase))
					chapters = file;
				else if (file.EndsWith(tagsFileName, StringComparison.OrdinalIgnoreCase))
					tags = file;
				else if (Formats.IsSubtitleFile(file))
					subtitles.Add(file);
			}

			mkvs.Add(new ContainerAttachments(chapters, tags, subtitles, fonts));
		}

		return mkvs;
	}

	private List<ContainerAttachments> FetchAttachmentsF(string dir)
	{
		var fonts = new List<string>();
		// Should be just one directory
		foreach (string subDir in Directory.GetDirectories(dir))
		{
			if (Utils.ContainsFontFiles(subDir))
				fonts.AddRange(Directory.GetFiles(subDir));
		}

		return Directory
		      .EnumerateFiles(dir)
		      .Where(Formats.IsSubtitleFile)
		      .OrderBy(file => file)
		      .Select(file => new ContainerAttachments(file, fonts))
		      .ToList();
	}
}