﻿using System;
using MKVToolNixAutomation.Enums;

namespace MKVToolNixAutomation.Data;

public class MKVException : Exception
{
	private ErrorType errorType;

	public MKVException(ErrorType errorType)
	{
		this.errorType = errorType;
	}

	public override string ToString()
	{
		string result = "Error: ";
		result += errorType switch
		{
			ErrorType.MKVToolNixNotSet               => "No MKVToolNix directory selected",
			ErrorType.MKVToolNixInvalid              => "Invalid MKVToolNix directory",
			ErrorType.VideosNotSet                   => "No videos selected",
			ErrorType.VideosAttachmentsMismatch      => "The number of videos and attachments does not match",
			ErrorType.AttachmentsDirNotSet           => "No attachments directory selected",
			ErrorType.AttachmentsDirInvalid          => "The attachments directory is invalid",
			ErrorType.AttachmentsDirEmpty            => "The attachments directory is empty",
			ErrorType.AttachmentsDirUnknownStructure => "The attachments directory structure is unknown",
			ErrorType.ChaptersFileNameEmpty          => "The 'chapters' file name cannot be empty",
			ErrorType.TagsFileNameEmpty              => "The 'tags' file name cannot be empty",
			_                                        => throw new ArgumentOutOfRangeException(nameof(errorType), errorType, null)
		};

		return result;
	}
}