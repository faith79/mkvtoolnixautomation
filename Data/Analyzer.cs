﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using MKVToolNixAutomation.Enums;
using MKVToolNixAutomation.Helpers;
using MKVToolNixAutomation.Models;

namespace MKVToolNixAutomation.Data;

public class Analyzer
{
	private readonly MKVToolNix mkvtoolnix;

	public Analyzer(MKVToolNix mkvtoolnix)
	{
		this.mkvtoolnix = mkvtoolnix;
	}

	public async Task<List<ContainerMetadata>> Load(IEnumerable<string> videoFilePaths)
	{
		var metadataList = new List<ContainerMetadata>();
		foreach (string filePath in videoFilePaths)
		{
			var metadata = await GetContainerMetadata(filePath);
			metadataList.Add(metadata);
			Logger.WriteLine($"Loaded `{Path.GetFileName(metadata.FilePath)}`", LogLevel.Info);
		}

		return metadataList;
	}

	public static Track GetDefaultTrack(IReadOnlyCollection<Track> tracks)
	{
		//there will always be an empty track so calling First() is fine
		return tracks.FirstOrDefault(track => track.Properties.IsDefault) ?? tracks.First(track => track.IsEmpty);
	}

	public static List<Track> GetTracksOfType(IEnumerable<ContainerMetadata?>? metadataList, TrackType type)
	{
		var tracks = metadataList?
		            .FirstOrDefault()?.Tracks
		            .Where(track => track.Type == type)
		            .ToList() ?? new List<Track>();
		tracks.Add(Track.Empty());

		return tracks;
	}

	private async Task<ContainerMetadata> GetContainerMetadata(string videoFilePath)
	{
		using var mkvmerge = new Process
		{
			StartInfo =
			{
				FileName = mkvtoolnix.Merge,
				RedirectStandardOutput = true,
				CreateNoWindow = true,
				UseShellExecute = false,
				Arguments = $"-J \"{videoFilePath}\""
			}
		};
		mkvmerge.Start();

		string json = await mkvmerge.StandardOutput.ReadToEndAsync();
		return JsonSerializer.Deserialize<ContainerMetadata>(json)!;
	}
}