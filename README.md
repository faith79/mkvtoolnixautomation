## Explanation of the different directory structures
The number of files and their names are irrelevant in most cases<br>
The directory containing fonts does not have to be called "attachments"<br>
Not all files need to be present<br>
For example structure A does not require a chapters.xml file to be recognized as such

# Structure A
+ <b>Main directory</b>
    - chapters.xml
    - tags.xml
    - track1.ass
    - track2.srt
    - OpenSans-Semibold.ttf
    - CantoraOne-Regular_0.ttf

# Structure B
+ <b>Main directory</b>
    + <b>attachments</b>
        - OpenSans-Semibold.ttf
        - CantoraOne-Regular_0.ttf
    - chapters.xml
    - tags.xml
    - track1.ass
    - track2.srt

# Structure C
+ <b>Main directory</b>
    + <b>Directory 1</b>
        - chapters.xml
        - tags.xml
        - track1.ass
        - track2.srt
        - OpenSans-Semibold.ttf
        - CantoraOne-Regular_0.ttf
    + <b>Directory 2</b>
        - chapters.xml
        - tags.xml
        - track1.ass
        - track2.srt
        - OpenSans-Semibold.ttf
        - CantoraOne-Regular_0.ttf
    + <b>...</b>

# Structure D
+ <b>Main directory</b>
    + <b>Directory 1</b>
        + <b>attachments</b>
            - OpenSans-Semibold.ttf
            - CantoraOne-Regular_0.ttf
        - chapters.xml
        - tags.xml
        - track1.ass
        - track2.srt
    + <b>Directory 2</b>
        + <b>attachments</b>
            - OpenSans-Semibold.ttf
            - CantoraOne-Regular_0.ttf
        - chapters.xml
        - tags.xml
        - track1.ass
        - track2.srt
    + <b>...</b>

# Structure E
+ <b>Main directory</b>
    + <b>Directory 1</b>
        - chapters.xml
        - tags.xml
        - track1.ass
        - track2.srt
    + <b>Directory 2</b>
        - chapters.xml
        - tags.xml
        - track1.ass
        - track2.srt
    + <b>...</b>
    + <b>attachments</b>
        - OpenSans-Semibold.ttf
        - CantoraOne-Regular_0.ttf

# Structure F
+ <b>Main directory</b>
    + <b>attachments</b>
        - OpenSans-Semibold.ttf
        - CantoraOne-Regular_0.ttf
    - episode1_subs.ass
    - episode2_subs.ass
    - episode3_subs.ass
    - ...