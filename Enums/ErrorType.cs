﻿namespace MKVToolNixAutomation.Enums;

public enum ErrorType
{
	MKVToolNixNotSet,
	MKVToolNixInvalid,
	VideosNotSet,
	VideosAttachmentsMismatch,
	AttachmentsDirNotSet,
	AttachmentsDirInvalid,
	AttachmentsDirEmpty,
	AttachmentsDirUnknownStructure,
	ChaptersFileNameEmpty,
	TagsFileNameEmpty
}