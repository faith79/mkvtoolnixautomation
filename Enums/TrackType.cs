﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MKVToolNixAutomation.Enums;

[JsonConverter(typeof(TrackTypeJsonConverter))]
public enum TrackType
{
	Empty,
	Video,
	Audio,
	Subtitles,
	Unknown
}

public class TrackTypeJsonConverter : JsonConverter<TrackType>
{
	public override TrackType Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
	{
		return reader.GetString() switch
		{
			"video"     => TrackType.Video,
			"audio"     => TrackType.Audio,
			"subtitles" => TrackType.Subtitles,
			_           => TrackType.Unknown
		};
	}

	public override void Write(Utf8JsonWriter writer, TrackType value, JsonSerializerOptions options)
	{
		// unused
	}
}