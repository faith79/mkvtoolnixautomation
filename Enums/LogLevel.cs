﻿namespace MKVToolNixAutomation.Enums;

public enum LogLevel
{
	Info,
	Warning,
	Error,
	Success
}